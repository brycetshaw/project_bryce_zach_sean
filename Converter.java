public class Converter {
	//Your names go here:
	/** Bryce Shaw
	 * Zachary Graham
	 * Sean Watters
	 * **/

	private double celsiusToFahrenheit(double C){
		return (C * 1.8) + 32;

	}

	private double fahrenheitToCelsius(double F){
		return (F - 32)/1.8;

	}

	public static void main(String[] args) {
        System.out.println("180 degC is equal to " + celsiusToFahrenheit(180) + " degF.");
        System.out.println("250 degF is equal to " + fahrenheitToCelsius(250) + " degC.");

	}
}
